export const state = () => ({
  isApproved: false,
})

export const mutations = {
  approve(state) {
    if (process.browser) localStorage.setItem('agreedToDisclaimer', true)
    state.isApproved = true
  },
  checkDisclaimerAgreed(state) {
    if (process.browser)
      if (localStorage.getItem('agreedToDisclaimer')) state.isApproved = true
  },
}

export const actions = {}

export const getters = {
  isApproved: (state) => state.isApproved,
}
