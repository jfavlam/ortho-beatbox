import pkg from './package.json'

export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  publicRuntimeConfig: {
    version: pkg.version,
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Ortho Beatbox',
    htmlAttrs: {
      lang: 'fr',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'msapplication-TileColor', content: '#ffffff' },
      { name: 'theme-color', content: '#ffffff' },
      {
        hid: 'description',
        name: 'description',
        content:
          'Ortho Beatbox est une application ludique destinées aux orthophonistes',
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'apple-touch-icon',
        sizes: '180x180',
        href: '/apple-touch-icon.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: '/favicon-32x32.png',
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: '/favicon-16x16.png',
      },
      {
        rel: 'mask-icon',
        color: '#6D28D9',
        href: '/safari-pinned-tab.svg',
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    // https://github.com/nuxt-community/router-module
    // '@nuxtjs/router',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'fr',
      name: 'Ortho Beatbox',
      short_name: 'Ortho Beatbox',
      background_color: '#ffffff',
      theme_color: '#6D28D9',
      // start_url: '/',
      display: 'standalone',
    },
    meta: {
      charset: 'utf-8',
      mobileApp: true,
      // mobileAppIOS: true,
      name: 'Ortho Beatbox',
      description:
        'Ortho Beatbox est une application ludique destinées aux orthophonistes',
      theme_color: '#6D28D9',
      lang: 'fr',
    },
    // workbox: {
    //   offlineAssets: [
    //     '/sounds/p.wav',
    //     '/sounds/pr.wav',
    //     '/sounds/ps.wav',
    //     '/sounds/t.wav',
    //     '/sounds/tr.wav',
    //     '/sounds/ts.wav',
    //     '/sounds/k.wav',
    //     '/sounds/kr.wav',
    //     '/sounds/ks.wav',
    //   ],
    // },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  buildDir: 'dist',

  // Generate Configuration: https://nuxtjs.org/docs/2.x/configuration-glo
  generate: {
    dir: 'dist',
    // fallback: true,
  },

  // server: {
  //   host: '0', // Serving local IP rather than localhost
  //   port: '3000',
  // },
}
