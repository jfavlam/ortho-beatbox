# Ortho Beatbox

[![Netlify Status](https://api.netlify.com/api/v1/badges/977f3ed3-ba0e-4af9-9f07-7ab60db6d524/deploy-status)](https://app.netlify.com/sites/ortho-beatbox/deploys)

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
